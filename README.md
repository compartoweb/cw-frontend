# CW Frontend

## Install
```bash
$ bower install git+ssh://git@bitbucket.org/compartoweb/cw-frontend.git#v1.x --save
```

## References
* http://mvcss.io/
* https://en.bem.info/methodology/

## Docs
* http://sassdoc.com/
